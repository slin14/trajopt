# TrajOpt

## Installation

Note: OpenRave, a dependency, does not work with more recent versions of Ubuntu (e.g. bionic beaver 18.04).
See instructions to install OpenRave below, then follow the installation [instructions](http://rll.berkeley.edu/trajopt/doc/sphinx_build/html/install.html) from TrajOpt.

### OpenRave Installation

There are [scripts](https://github.com/crigroup/openrave-installation) from the crigroup that may work.

#### Check your boost version before proceeding
My boost version is 1.58.0 and running these scripts worked. It didn't work for Jesse and downgraded his Boost to 1.30 or something. 

I recommend making a copy of your catkin workspace, `catkin_ws` or whatever it is called, so you have a backup copy. For example, if you named your cloned workspace `caris_ws`, then

```bash
cd caris_ws
git clone https://github.com/crigroup/openrave-installation.git
cd openrave-installation

```

Follow their instructions to run the scripts in the following order Please read the section below BEFORE you run.

```bash
./install-dependencies.sh
./install-osg.sh
./install-fcl.sh
./install-openrave.sh
```

After running EACH line above, test if your catkin workspace still functions by doing some or all of the following: (This can make fixing damages easier if anything bad happens)
- data processing
- `catkin build`

### Gurobi Installation
### Gurobi
Gurobi is an optimization library. [Register](https://www.gurobi.com/downloads/gurobi-optimizer-eula/) for an account - it's free for academics. The cwl@student.ubc.ca email works. Setting the environment path is best done in your ~/.bashrc. Make sure to read the Gurobi README/release notes on installation procedures. If you don't also add the `bin` folder to your PATH, go to the `bin` folder to use `./grbgetkey`.

Set GUROBI_HOME and add to your ~/.bashrc
```bash
echo "export GUROBI_HOME=/home/sophie/gurobi910/linux64" >> ~/.bashrc
```

Instead of cloning from the trajopt repo linked in the [instructions](http://rll.berkeley.edu/trajopt/doc/sphinx_build/html/install.html), clone from the [trajopt_ros repo](https://github.com/ros-industrial-consortium/trajopt_ros)
```bash
git clone https://github.com/ros-industrial-consortium/trajopt_ros.git
```
Install using rosinstall
```bash
cd caris_ws
rosinstall build_trajopt /opt/ros/melodic /home/sophie/caris_ws/trajopt_ros/dependencies.rosinstall
``` 

Set up your environment (You can also add this to your ` ~/.bashrc`. To edit your `~/.bashrc`: `xdg-open ~/.bashrc`)
```bash
source /home/sophie/caris_ws/build_trajopt/setup.bash
```

Set PYTHONPATH and add to your `~/.bashrc`
```bash
echo "export PYTHONPATH=/home/sophie/caris_ws/trajopt" >> ~/.bashrc
echo "export PYTHONPATH="/home/sophie/caris_ws/build_trajopt/lib:$PYTHONPATH"" >> ~/.bashrc
```

`cd` into your TrajOpt workspace and build
```bash
cd build_trajopt
catkin build
```

## Errors
Note: `catkin build` seems to re-build the pr2_box packages only, not any of the new TrajOpt packages...

So I tried the following `catkin make` comamnd, which give the error that cmake can't find `trajopt_sco`  but it's should be just 1 directory up.
```bash
catkin_make --source /home/sophie/caris_ws/trajopt_ros/trajopt
```
